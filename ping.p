load 'common.cfg'
load 'line-graphs.cfg'
set output outputdir."/ping.png"

plot file using 1:4 with points title "Ping in ms" lc rgb"black"

load 'common.cfg'
load 'line-graphs.cfg'
set output outputdir."/uplink.png"

plot file using 1:3 smooth unique title "Upload in MBit/s" lc rgb"black"

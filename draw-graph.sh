#!/usr/bin/bash
cd /opt/netzwerkspeedtest
mysql -u speedtest -pyDr1tB2gqqmAxxN8 speedtest < sql/cleanup.sql # This deletes all failed datasets
mysql -B -u speedtest -pyDr1tB2gqqmAxxN8 speedtest < sql/all-data.sql | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > /tmp/speed_all_data_pre.csv
mysql -B -u speedtest -pyDr1tB2gqqmAxxN8 speedtest < sql/day-avg-min-max.sql | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > /tmp/speed_avg_min_max_day_pre.csv
mysql -B -u speedtest -pyDr1tB2gqqmAxxN8 speedtest < sql/sponsors.sql | sed "s/'/\'/;s/\t/\",\"/g;s/^/\"/;s/$/\"/;s/\n//g" > /tmp/sponsors.csv

sed -n '0~20p' /tmp/speed_all_data_pre.csv > /tmp/speed_all_data.csv
sed -n '0~5p' /tmp/speed_avg_min_max_day_pre.csv > /tmp/speed_avg_min_max_day.csv

for f in *.p
do
	gnuplot $f
done

load 'common.cfg'
load 'min-max-avg.cfg'
set output outputdir."/min-max-avg-upload.png"
set ylabel "Download in MBit/s"

plot file using 5:xtic(1) lt rgb"green" title "Minimalwert",\
     file using 6 lt rgb"blue" title "Maximalwert", \
     file using 7 lt rgb"red" smooth unique title "Durchschnitt"

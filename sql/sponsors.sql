SELECT
	COUNT(sponsor) as counter,
	sponsor
FROM
	speedtest
GROUP BY
	sponsor
ORDER BY
	counter
;

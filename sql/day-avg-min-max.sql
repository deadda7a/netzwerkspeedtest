SELECT
	DATE(timestamp) as date,
	ROUND(MIN(download)/1024/1024, 2) AS min_download,
	ROUND(MAX(download)/1024/1024, 2) AS max_download,
	ROUND(AVG(download)/1024/1024, 2) AS avg_download,

	ROUND(MIN(upload)/1024/1024, 2) AS min_upload,
	ROUND(MAX(upload)/1024/1024, 2) AS max_upload,
	ROUND(AVG(upload)/1024/1024, 2) AS avg_upload,

	ROUND(MIN(ping), 2) AS min_ping,
	ROUND(MAX(ping), 2) AS max_ping,
	ROUND(AVG(ping), 2) AS avg_ping
FROM
	speedtest
GROUP BY
	DATE(timestamp)

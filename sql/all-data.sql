SELECT
  UNIX_TIMESTAMP(timestamp),
  ROUND(download/1024/1024, 2),
  ROUND(upload/1024/1024,2),
  ROUND(ping,2)
FROM speedtest;

load 'common.cfg'
load 'min-max-avg.cfg'
set output outputdir."/min-max-avg-ping.png"
set ylabel "Download in MBit/s"

plot file using 8:xtic(1) lt rgb"green" title "Minimalwert",\
     file using 9 lt rgb"blue" title "Maximalwert", \
     file using 10 lt rgb"red" smooth unique title "Durchschnitt"
